<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>



	<!-- HERO SECTION -->
	<section class='hero person_hero'>
		<div class='text_box'>
			<h1><?php the_field('name'); ?></h1>
			<p><?php the_field('position'); ?></p>
		</div>
		<div class='circle_wrapper circle_logo person'>
			<img src="<?php the_field('image'); ?>">
		</div>
	</section>

	<section class='quotes'>
		<span>"</span><p><?php the_field('quote'); ?>"</p>
	</section>
	<!-- HERO SECTION ENDS -->


	<!-- PERSON CONTENT -->
	<section class='bio'>
		<h2>About Marc</h2>
        <?php the_field('bio'); ?>
	</section>
	<!-- PERSON CONTETN END -->
	




<?php
get_footer();
