<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>

	<!-- FOOTER SECTION STARTS -->
	<footer>
        
		 
		<div class='footer_holder'>
<!--
            			<div class="updates newsletter">
				<div class='left_center'>
                    <a href="/"><p class='logo'>THE NEXT TEXT</p></a> 
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.jpg">


				</div>
			</div>
-->
            
			<div class="links">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1'
                    ) );
                ?>
			</div>

            

		</div>
	</footer>
	<!-- FOOTER SECTION ENDS -->


<script>
$(".scroll_down").click(function(e) {
    e.preventDefault();
    $('html,body').animate({
        scrollTop: $(".summary_icons").offset().top},
        'slow');
});
</script>
	

<?php wp_footer(); ?>

</body>
</html>
