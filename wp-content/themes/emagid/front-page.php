<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: Front Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>



	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1><?php the_field('hero_title'); ?></h1>
                <p><?php the_field('hero_subtitle'); ?></p>
                <a href='<?php the_field('button_link'); ?>' class='button scroll_down'>GET STARTED</a>
            </div>
        </div>
	</section>
<!--
    <section class="small_hero">
            <div class='text_box'>
                <h4><?php the_field('red_banner_text'); ?></h4>
                <a href="/my-account"><h4>Register</h4></a>
            </div>
    </section>
-->

	<!-- HERO SECTION END -->

	<!-- GROWTH SECTION -->
	<section class='growth' style="padding-bottom:50px;">
		<div class="head_text">
			<h2><span>Most Recent Texts</span></h2>
		</div>
        <div class="classes">
                <div class="class_box div_4">
                    <div class="wrapper">
                    <h4>Should I text him</h4>
                    <p>He hadnt texted me for almost a week(unusual) I decided not to overreact and text him myself asking how it was going. No reply. Next day I call him, d...</p>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/speech.png">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb_share.png">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter_share.png">
                        </div>
                </div>
            
            <div class="class_box div_4">
                    <div class="wrapper">
                    <h4>Should I text him</h4>
                    <p>He hadnt texted me for almost a week(unusual) I decided not to overreact and text him myself asking how it was going. No reply. Next day I call him, d...</p>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/speech.png">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb_share.png">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter_share.png">
                        </div>
                </div>
            
            <div class="class_box div_4">
                    <div class="wrapper">
                    <h4>Should I text him</h4>
                    <p>He hadnt texted me for almost a week(unusual) I decided not to overreact and text him myself asking how it was going. No reply. Next day I call him, d...</p>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/speech.png">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb_share.png">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter_share.png">
                        </div>
                </div>
            
            <div class="class_box div_4">
                    <div class="wrapper">
                    <h4>Should I text him</h4>
                    <p>He hadnt texted me for almost a week(unusual) I decided not to overreact and text him myself asking how it was going. No reply. Next day I call him, d...</p>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/speech.png">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb_share.png">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter_share.png">
                        </div>
                </div>

            </div>

    </section>
	<!-- GROWTH SECTION END -->

<!-- ICON SECTION -->
<section class="summary_icons">
    <div class="wrapper">
        <h2><span>Top Categories</span></h2>
        <div class="s_icon">
            <a href="/">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/friendship.png">
                <p>romance</p>
            </a>
        </div>
        <div class="s_icon">
            <a href="/">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gift.png">
                <p>friendship</p>
            </a>
        </div>

        <div class="s_icon">
            <a href="/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/travel.png">
            <p>travel</p>
            </a>
        </div>
        <div class="s_icon">
            <a href="/">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/all.png">
                <p>Everything Else</p>
            </a>
        </div>
        <div class="s_icon">
            <a href="/">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/life.png">
                <p>Life</p>
            </a>
        </div>
    </div>
</section>

<!-- ICON SECTION END -->


<!-- MIDBANNER SECTION -->
<!--
<section class="midbanner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/capital.jpeg)">
</section>
-->

<!-- MIDBANNER SECTION END -->

	<!-- QUOTE SECTION -->
<!--
	<div class="quote_slider">
		<div class='quotes'>
			<img src="<?php the_field('eric_image'); ?>">
            <div class="quote_text">
                <h3>Eric Schleien</h3>
                <p><?php the_field('eric_text'); ?></p>
            </div>
		</div>
        <div class='quotes'>
			<img src="<?php the_field('john_image'); ?>">
            <div class="quote_text">
                <h3>John King</h3>
                <p><?php the_field('john_text'); ?></p>
            </div>
		</div>
	</div>
-->
	<!-- QUOTE SECTION END  -->


	<!-- GROWTH SECTION -->
	<section class='growth'>
		<div class="head_text">
			<h2><span>Who We Are</span></h2>
		</div>
        <div class="classes">
                <div class="class_box">
                    <h4><?php the_field('headline'); ?></h4>
                    <p>The Next Text was founded in 2011 by two friends who probably should’ve created it half a decade ago and saved the text obsessed masses from </p>
                    <a href='/about/' class='button'> READ MORE </a>
                </div>

            </div>
        </div>

    </section>
	<!-- GROWTH SECTION END -->


<!-- CTABANNER SECTION -->
<!--
<section class="ctabanner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/ctabanner.jpg)">
    <div class="overlay">
        <div class='text_box'>
            <a href='/about' class='button'> HOW IT WORKS</a>
        </div>
    </div>
</section>
-->

<!-- CTABANNER SECTION END -->





  <script type="text/javascript">
    $(document).ready(function(){
      $('.quote_slider').slick({
        autoplay:true,
		autoplaySpeed:2500,
		dots: true,
          arrows: true
      });
    });
  </script>
<script>
$(".scroll_down").click(function() {
    $('html,body').animate({
        scrollTop: $(".summary_icons").offset().top},
        'slow');
});
</script>
<?php
get_footer();
